let trainer = {
    name: "Ash Ketchum",
    age: "10",
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function() {
       return "Pikachu! I choose you!";
    }
};
console.log(trainer);
console.log("Result of dot notation:")
console.log(trainer.name);
console.log("Result of square brakcet notation:")
console.log(trainer.pokemon);
console.log("Result of talk method")
console.log(trainer.talk());






function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = level * 2;
  this.attack = level;
  this.tackle = function(target) {
    console.log(this.name + " tackled " + target.name);
    target.health -= this.attack;
    console.log(target.name + "'s health is now reduced to " + target.health);
    if (target.health <= 0) {
      target.faint();
    }
  };
  this.faint = function() {
    console.log(this.name + " fainted");
  };
}
let charmander = new Pokemon("Pikachu", 12);
let squirtle = new Pokemon("Geodude", 8);
let bulbasaur = new Pokemon("Mewtwo", 100);

console.log(charmander);
console.log(squirtle);
console.log(bulbasaur);


charmander.tackle(squirtle);
console.log(charmander);

bulbasaur.tackle(squirtle);
console.log(squirtle);
